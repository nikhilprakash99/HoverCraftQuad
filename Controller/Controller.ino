#define ThrPin 2
#define YawPin 3
#define LeftMotorPin 9
#define RightMotorPin 10

#define MaxVelPWM 750
#define MaxAngPWM 500

int i=0;

int THR_PWM,YAW_PWM;
long int throttle,yaw;
long int LeftMotorPWM , RightMotorPWM ;

void setup() {
  pinMode(ThrPin, INPUT);
  pinMode(YawPin, INPUT);
  pinMode(LeftMotorPin, OUTPUT);
  pinMode(RightMotorPin, OUTPUT);
  Serial.begin(115200);
}

void loop() {

  THR_PWM = pulseIn(ThrPin,HIGH,1000000);
  YAW_PWM = pulseIn(YawPin,HIGH,1000000);

  throttle = (THR_PWM - 1000);
  yaw      = (YAW_PWM -1500);

  LeftMotorPWM  = 1000 + (throttle*MaxVelPWM)/1000 + (yaw*MaxAngPWM)/1000;
  RightMotorPWM = 1000 + (throttle*MaxVelPWM)/1000 - (yaw*MaxAngPWM)/1000;

  analogWrite(LeftMotorPin, ((LeftMotorPWM*255)/2000));
  analogWrite(RightMotorPin, ((RightMotorPWM*255)/2000));

  Serial.println(i++);
  Serial.print("Throttle PWM : ");
  Serial.println(THR_PWM);
  Serial.print("Yaw PWM : ");
  Serial.println(YAW_PWM);
  Serial.print("Motor PWMs : ");
  Serial.print(LeftMotorPWM);
  Serial.print(" , ");
  Serial.println(RightMotorPWM);
}
