// #define ThrPin 2
 #define YawPin 3
#define LeftMotorPin 5
#define RightMotorPin 6

#define MaxVelPWM 750
#define MaxAngPWM 150

int i=0;

volatile int THR_PWM;
volatile int prev_thr_time;
float throttle,yaw;

float LeftMotorPWM , RightMotorPWM ;

void setup() {
  // pinMode(ThrPin, INPUT);
  pinMode(YawPin, INPUT);
  pinMode(LeftMotorPin, OUTPUT);
  pinMode(RightMotorPin, OUTPUT);
  attachInterrupt(0, thr_rising, RISING);
  Serial.begin(9600);
}

void loop() {

  //THR_PWM = pulseIn(ThrPin,HIGH,1000000);
  YAW_PWM = pulseIn(YawPin,HIGH,1000000);

  throttle = (THR_PWM - 1000)/1000.0;
  yaw      = (YAW_PWM -1500)/1000.0;

  LeftMotorPWM  = 1000 + (throttle*MaxVelPWM)+ (yaw*MaxAngPWM);
  RightMotorPWM = 1000 + (throttle*MaxVelPWM)- (yaw*MaxAngPWM);

  analogWrite(LeftMotorPWM, (int)((LeftMotorPWM/2000)*255));
  analogWrite(LeftMotorPWM, (int)((RightMotorPWM/2000)*255));

  Serial.println(i++);
  Serial.print("Throttle PWM : ");
  Serial.print(THR_PWM);
  Serial.print("\nYaw PWM : ");
  Serial.println(YAW_PWM);
}

void thr_rising() {
  attachInterrupt(0, thr_falling, FALLING);
  prev_thr_time = micros();
}

void thr_falling() {
  attachInterrupt(0, thr_rising, RISING);
  THR_PWM = micros()-prev_thr_time;
}
